FROM rust:1.72.0-slim-bullseye

RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        pkg-config \
        libssl-dev \
        git \
        ; \
    rm -rf /var/lib/apt/lists/*; \
    cargo install --version 0.3.0 licensure;
