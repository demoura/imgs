FROM rust:1.72.0-alpine3.18
RUN [ "apk", "add", "--no-cache", "git", "openssl-dev", "musl-dev", "perl", "make"]
